import React, { useState } from 'react';
import { Button, Menu, Drawer, Row } from "antd"
import Link from "next/link"
import { useRouter } from 'next/router';


const { SubMenu } = Menu;

export interface CMSMenuComponent {
  windowSize: { width: Number, height: Number },
  menus?: [
    { name: any, sequence: Number, link: any, subMenuItems?: any }
  ],
  cart?: boolean,
  openCart?: any,
  defaultMobileSize?: Number,
  // activeClassName?: string,
  className?: string
}

const MenuComponent = ({
  windowSize,
  menus,
  openCart,
  cart,
  defaultMobileSize = 768,
  // activeClassName = 'active-selected',
  className = 'navigation-menu'
}: CMSMenuComponent) => {

  const router = useRouter();
  const [drawer, setDrawer] = useState(false);

  // const activeMenu = data.find(d => d.component == component) || data[0]
  const activeMenu = menus.find(menu => {
    if (menu.subMenuItems != null) {
      return menu.subMenuItems.find(subMenu => (
        subMenu.link == router.pathname
      ))
    }
    return menu.link == router.pathname
  })

  return (
    <section className={className}>
      {
        windowSize.width < defaultMobileSize ?
          <div className="p-r-20">
            <Button
              className="color-primary m-t-5"
              shape="circle"
              icon="menu"
              size="large"
              onClick={() => setDrawer(true)}
            />
            <div>
              <Drawer
                placement="left"
                closable={true}
                width={'100%'}
                onClose={() => setDrawer(false)}
                visible={drawer}
              >
                <Menu onClick={() => setDrawer(false)} selectedKeys={['Home']} mode="inline" className="m-t-40">
                  {menus.map((menu, index) =>
                    menu.subMenuItems
                      ? <SubMenu
                        key={`menu-${index}`}
                        className="padding-left-reduce"
                        title={
                          <div>
                            <a>{menu.name}
                            </a>
                          </div>
                        }
                      >
                        {menu.subMenuItems.map(subMenu =>
                          <Menu.Item key={subMenu.name}>
                            <Link href={subMenu.link || '#'}>
                              <div>
                                <a>{subMenu.name}</a>
                              </div>
                            </Link>
                          </Menu.Item>
                        )}
                      </SubMenu>
                      :
                      <Menu.Item key={menu.name} className="p-x-0">
                        <div>
                          <Link href={menu.link || '#'}>
                            <div>
                              <a>{menu.name}</a>
                            </div>
                          </Link>
                        </div>
                      </Menu.Item>
                  )}
                </Menu>

              </Drawer>
            </div>
          </div>
          :
          <Menu onClick={() => { }} selectedKeys={['Home']} mode="horizontal" className="m-t-5">
            {menus.map((menu, index) =>
              menu.subMenuItems
                ? <SubMenu
                  key={`menu-${index}`}
                  title={
                    <div>
                      <a>{menu.name}
                      </a>
                    </div>
                  }
                >
                  {menu.subMenuItems.map(subMenu =>
                    <Menu.Item key={subMenu.name}>
                      <Link href={subMenu.link || '#'}>
                        <div>
                          <a>{subMenu.name}</a>
                        </div>
                      </Link>
                    </Menu.Item>
                  )}
                </SubMenu>
                :
                <Menu.Item key={menu.name} className="p-x-0">
                  <div>
                    <Link href={menu.link || '#'}>
                      <div>
                        <a >{menu.name}</a>
                      </div>
                    </Link>
                  </div>
                </Menu.Item>
            )}
            {
              cart &&
              <Menu.Item key="cart-key-0">
                <Button className="cart-btn" onClick={() => openCart()}>Cart</Button>
              </Menu.Item>
            }
          </Menu>
      }
    </section>
  )
}

export default MenuComponent