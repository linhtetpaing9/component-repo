import React, { createContext, useEffect, useState } from 'react'
import AOS from 'aos';
import Head from 'next/head'
import MetaTag from '../MetaTag'
import NavBar from '../NavigationBar'
import { useRouter } from 'next/router'
import { Layout } from 'antd';

const { Header, Content } = Layout;

export const LayoutContext: any = createContext({})

const WrappedLayout = ({ children, layoutContent }) => {
  const Router = useRouter();
  useEffect(() => {
    AOS.init();
  });
  const [windowSize, setWindowSize] = useState<any>({})
  const [adminMode, setAdminMode] = useState<boolean>(false)
  const [sider, setSider] = useState<string>("")
  const [order, setOrder] = useState<any>({ loading: true })
  const [customer, setCustomer] = useState<any>({ loading: true })
  const [, setPriceRules] = useState<any>({ loading: true })
  const [cart, setCart] = useState<any>(null)


  useEffect(() => {


    document.addEventListener("keydown", (event) => {
      if (event.ctrlKey && event.code === "Backquote") {
        setAdminMode(!adminMode)
      }
    })

    checkWindowSize();
    window.addEventListener("resize", checkWindowSize)
  }, [])

  const checkWindowSize = () => {
    setWindowSize({
      width: window.innerWidth,
      height: window.innerHeight
    })
  }

  const openCart = async () => {
    setSider("cart")
  }

  return (

    <LayoutContext.Provider
      value={{
        windowSize,
        adminMode,
        cart,
        customer,
        openCart,
        order,
        layoutContent
      }}
    >
      <Head>
        <MetaTag title={Router.pathname} />
      </Head>
      <Layout className="layout">
        <Header>
          <NavBar Router={Router} />
        </Header>
        <Content style={{ padding: '0 50px' }}>
          {children}
        </Content>
      </Layout>
    </LayoutContext.Provider>
  );
}
export default WrappedLayout;