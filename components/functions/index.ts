function addHtmlSpacing(value: string): string {
  if (value != null) {
    return value.replace(/\n/g, "<br/>");
  }
  return value;
}

function chunk(inputArray: [], perChunk: number) {
  var i, j, result = [];
  for (i = 0, j = inputArray.length; i < j; i += perChunk) {
    result.push(inputArray.slice(i, i + perChunk))
  }
  return result;
}

function isObjectEmpty(obj) {
  return Object.keys(obj).length === 0;
}

function cleanUrl(url: string) {
  if (!url) return null
  const alpanumericHyphened = url
    .toLowerCase()                    // convert to lowercase
    .replace(/[^a-zA-Z0-9]/g, ' ')     // replace non alpanumerics with spaces
    .trim()                           // remove trailing spaces
    .replace(/( )\1{1,}/g, ' ')        // reduce consecutive spaces
    .replace(/ /g, '-')                // replace spaces with hyphens
  return (alpanumericHyphened || "-") // use hyphen in case of ""
}


// need more improvement with type
function getContent(content = {}) {
  const entryContent = Object.entries(content)
  return {
    blockId: (blockId) => {
      const result: any = entryContent
        .find(([code]) => code.toLowerCase() === blockId)
      return result || []
    },
    // data are not sorted need to fix
    blockIds: (blockIds) => {
      const result: any = entryContent
        .filter(([code]) => blockIds.indexOf(code.toLowerCase()) > -1)
      return result || []
    },
    tag: (tag) => {
      const result: any = entryContent
        .filter(([, content]: any) => content?.tags?.indexOf(tag) > -1)
      return {
        sortWith: (item) => {
          return {
            lowestToHighest: () => {
              const sortedItems = result.sort(([, a], [, b]) => (
                parseInt(a[item] || 0) - parseInt(b[item] || 1)
              ))
              return sortedItems || []
            },
            highestToLowest: () => {
              const sortedItems = result.sort(([, a], [, b]) =>
                parseInt(b[item] || 0) - parseInt(a[item] || 1)
              )
              return sortedItems || []
            }
          }
        }
      }
    }
  }
}

async function getDocument(path: string, domain: string = 'www.silkwaytravelasia.com') {
  let url = domain != null ? resolvePath(domain, path) : path
  console.log('url', url)
  let resp = await fetch(`https://asia-east2-headless-cms-292305.cloudfunctions.net/pageDetails?url=${encodeURIComponent(url)}`)
  return await resp.json()
}

function resolvePath(...args: string[]): string {
  return args.map((token: any) => token.slice(0, 1) === '/' ? token.slice(1) : token)
    .map((token: any) => token.slice(-1) === '/' ? token.slice(0, -1) : token)
    .join('/')
}


export default {
  cleanUrl,
  isObjectEmpty,
  chunk,
  getContent,
  addHtmlSpacing,
  getDocument
}