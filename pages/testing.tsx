import React, { useContext } from 'react';
import MenuComponent from '../components/cms/MenuComponent';
import { getContent } from '../components/functions';
import { LayoutContext } from '../components/layouts/WrappedLayout';

const Testing = () => {
  const { openCart, windowSize, layoutContent } = useContext(LayoutContext)

  const [, header] = getContent(layoutContent).blockId('header')
  const menus = header.content
  console.log({menus})
  return (
    // <h1>Hello</h1>
    <MenuComponent 
      windowSize={windowSize}
      menus={menus}
    />
  )
}

export default Testing;