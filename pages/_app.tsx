import React from 'react'
import App from 'next/app'
import { loadGetInitialProps } from 'next-server/dist/lib/utils'
import WrappedLayout from '../components/layouts/WrappedLayout'
import * as NProgress from 'nprogress/nprogress'
import Router from 'next/router'
import '../styles/index.less'
import { getDocument } from '../components/functions'

Router.events.on('routeChangeStart', () => NProgress.start())
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    const pageProps = await loadGetInitialProps(Component, ctx)
    const layoutContent = await getDocument('layout', 'www.silkwaytravelasia.com')
    return { pageProps, layoutContent }
  }


  render() {
    const { Component, pageProps, layoutContent } = this.props as any
    return (
      <WrappedLayout layoutContent={layoutContent}>
        <Component {...pageProps} />
      </WrappedLayout>
    )
  }
}

export default MyApp